--
-- Database: "test"
--
CREATE TABLE IF NOT EXISTS dilo (
  id int(11) NOT NULL AUTO_INCREMENT,
  isLocked int(11) NOT NULL,
  entered int(11) NOT NULL,
  PRIMARY KEY (id)
) DEFAULT CHARSET=latin1;

INSERT INTO dilo ('isLocked', 'entered') VALUES(1, 0);