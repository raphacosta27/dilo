document.addEventListener("DOMContentLoaded", function(event) {
    let lockedVariable = document.getElementById("islocked");

    let isLockedApplication;
    let enteredApplication;
    fetch("http://18.221.95.0/getStatus", {
        method: 'GET'
    }).then((resp) => resp.json()) // Transform the data into json
    .then(function(data) {
        let result = data["0"];
        isLockedApplication = result.isLocked;
        enteredApplication = result.entered;

        

        if(isLockedApplication == '0'){
            $("#islocked").append("<h4 style='color: red'> ABERTA </h4>");
            $(".mySwitch")["0"].checked = true;
        }
        else{
            $("#islocked").append("<h4 style='color: green'> FECHADA </h4>");
            $(".mySwitch")["0"].checked = false;
        }

        if(enteredApplication == '0'){
            $("#entered").append("<h4 style='color: green'> VAZIO </h4>");
        }
        else{
            $("#entered").append("<h4 style='color: red'> PRESENÇA DETECTADA </h4>");
            $("#placeButton").append("<button class='btn waves-effect waves-light' type='submit' id='okButton'>Ok</button> <label for='okButton'>Clique no botao se voce sabe quem é </label>")

            $("#okButton").click(function(){
                fetch("http://18.221.95.0/getStatus", {
                    method: 'GET'
                }).then((resp) => resp.json()) // Transform the data into json
                .then(function(data) {
                    let result = data["0"];
                    console.log("ok button");
                    console.log(result);
                    console.log("Result isLocked: " + result.isLocked)
                })
        

                console.log("Vai mandar: " + isLockedApplication +" e entered = 0");
                fetch("http://18.221.95.0/setStatus/?islocked=" + isLockedApplication +"&entered=0", {
                    method: 'GET'
                }).then(response => {
                    location.reload();
                })
            });
        
        }
        console.log(result);
    })
    
    setInterval(function(){ 
        
        fetch("http://18.221.95.0/getStatus", {
            method: 'GET'
        }).then((resp) => resp.json()) // Transform the data into json
        .then(function(data) {
            let result = data["0"];
            let isLockedInterval = result.isLocked;
            let enteredInterval = result.entered;
            if(isLockedApplication != isLockedInterval || enteredApplication != enteredInterval){
                location.reload();
            }
        })
    }, 5000);

    $(".mySwitch").on("change", function(){
        fetch("http://18.221.95.0/getStatus", {
            method: 'GET'
        }).then((resp) => resp.json()) // Transform the data into json
        .then(function(data) {
            let result = data["0"];
            enteredApplication = result.entered;
        })

        let islockedPost = 1;
        if($(".mySwitch")["0"].checked){
            islockedPost = 0;
        }

        console.log("Vai mandar");
        fetch("http://18.221.95.0/setStatus/?islocked="+islockedPost+"&entered="+enteredApplication, {
            method: 'GET'
        }).then(response => {
            location.reload();
        })
    })
  });
