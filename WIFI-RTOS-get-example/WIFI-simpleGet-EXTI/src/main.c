#include "asf.h"
#include "main.h"
#include <string.h>
#include "bsp/include/nm_bsp.h"
#include "driver/include/m2m_wifi.h"
#include "socket/include/socket.h"
#include "Stepper.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- WINC1500 weather client example --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL
	
/** Semaforo a ser usado pela task led */
QueueHandle_t xQueueMotor;

/** IP address of host. */
uint32_t gu32HostIp = 0;

/** TCP client socket handlers. */
static SOCKET tcp_client_socket = -1;

/** Receive buffer definition. */
static uint8_t gau8ReceivedBuffer[MAIN_WIFI_M2M_BUFFER_SIZE] = {0};

/** Wi-Fi status variable. */
static bool gbConnectedWifi = false;

/** Define if is in TX or RX mode */
static bool isTx = false;
static bool sendBuffer = false;

volatile uint32_t door = 0;
volatile uint32_t motion = 0;
volatile uint32_t invadida = 0;

/** Get host IP status variable. */
/** Wi-Fi connection state */
static uint8_t wifi_connected;

volatile uint32_t counter = 0;

/** Instance of HTTP client module. */
static bool gbHostIpByName = false;

/** TCP Connection status variable. */
static bool gbTcpConnection = false;

/** Server host name. */
static char server_host_name[] = MAIN_SERVER_NAME;

volatile TaskHandle_t xHandleStepper;

#define MOTOR1_PIO
#define MOTOR1_PIO_ID ID_PIOC
#define MOTOR1_PIO_PIN 13
#define MOTOR1_PIO_PIN_MASK (1 << MOTOR1_PIO_PIN)
#define MOTOR1_PIO PIOC

#define MOTOR2_PIO
#define MOTOR2_PIO_ID ID_PIOD
#define MOTOR2_PIO_PIN 11
#define MOTOR2_PIO_PIN_MASK (1 << MOTOR2_PIO_PIN)
#define MOTOR2_PIO PIOD

#define MOTOR3_PIO
#define MOTOR3_PIO_ID ID_PIOD
#define MOTOR3_PIO_PIN 26
#define MOTOR3_PIO_PIN_MASK (1 << MOTOR3_PIO_PIN)
#define MOTOR3_PIO PIOD

#define MOTOR4_PIO
#define MOTOR4_PIO_ID ID_PIOA
#define MOTOR4_PIO_PIN 24
#define MOTOR4_PIO_PIN_MASK (1 << MOTOR4_PIO_PIN)
#define MOTOR4_PIO PIOA

#define DOOR_SENSOR_PIO PIOC
#define DOOR_SENSOR_PIO_ID ID_PIOC
#define DOOR_SENSOR_PIN 19
#define DOOR_SENSOR_PIO_MASK (1 << DOOR_SENSOR_PIN) //Verde

#define MOTION_SENSOR_PIO PIOD
#define MOTION_SENSOR_PIO_ID ID_PIOD
#define MOTION_SENSOR_PIN 30
#define MOTION_SENSOR_PIO_MASK (1 << MOTION_SENSOR_PIN)  //Azul

#define TASK_WIFI_STACK_SIZE            (4096/sizeof(portSTACK_TYPE))
#define TASK_WIFI_STACK_PRIORITY        (tskIDLE_PRIORITY)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);



/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}


/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


/* 
 * Check whether "cp" is a valid ascii representation
 * of an Internet address and convert to a binary address.
 * Returns 1 if the address is valid, 0 if not.
 * This replaces inet_addr, the return value from which
 * cannot distinguish between failure and a local broadcast address.
 */
 /* http://www.cs.cmu.edu/afs/cs/academic/class/15213-f00/unpv12e/libfree/inet_aton.c */
int inet_aton(const char *cp, in_addr *ap)
{
  int dots = 0;
  register u_long acc = 0, addr = 0;

  do {
	  register char cc = *cp;

	  switch (cc) {
	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
	        acc = acc * 10 + (cc - '0');
	        break;

	    case '.':
	        if (++dots > 3) {
		    return 0;
	        }
	        /* Fall through */

	    case '\0':
	        if (acc > 255) {
		    return 0;
	        }
	        addr = addr << 8 | acc;
	        acc = 0;
	        break;

	    default:
	        return 0;
    }
  } while (*cp++) ;

  /* Normalize the address */
  if (dots < 3) {
	  addr <<= 8 * (3 - dots) ;
  }

  /* Store it if requested */
  if (ap) {
	  ap->s_addr = _htonl(addr);
  }

  return 1;    
}

void motion_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("---------------------\n");
	printf("sensor callback\n");
	if(motion == 0){
		motion = !motion;
		printf("Invadida\n");
		printf("status motion sensor: %d\n", motion);
		printf("isTx = true\n");
		isTx = true;
		sendBuffer = true;

	}
	else{
		printf("Esperar usuario dizer ok\n");
	}
	printf("---------------------\n");
}

void door_callback(void){
	sendBuffer = true;
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("---------------------\n");
	printf("door callback\n");
	isTx = true;
	door = !door;
	printf("status door sensor: %d\n", door);
	printf("isTx = true\n");
	printf("---------------------\n");
}


/**
 * \brief Callback function of IP address.
 *
 * \param[in] hostName Domain name.
 * \param[in] hostIp Server IP.
 *
 * \return None.
 */
static void resolve_cb(uint8_t *hostName, uint32_t hostIp)
{
	gu32HostIp = hostIp;
	gbHostIpByName = true;
	printf("resolve_cb: %s IP address is %d.%d.%d.%d\r\n\r\n", hostName,
			(int)IPV4_BYTE(hostIp, 0), (int)IPV4_BYTE(hostIp, 1),
			(int)IPV4_BYTE(hostIp, 2), (int)IPV4_BYTE(hostIp, 3));
}

/**
 * \brief Callback function of TCP client socket.
 *
 * \param[in] sock socket handler.
 * \param[in] u8Msg Type of Socket notification
 * \param[in] pvMsg A structure contains notification informations.
 *
 * \return None.
 */

static void socket_cb_rx(SOCKET sock, uint8_t u8Msg, void *pvMsg){
	/* Check for socket event on TCP socket. */
	if (sock == tcp_client_socket) {
		
		switch (u8Msg) {
			case SOCKET_MSG_CONNECT:
			{
				//printf("socket_msg_connect\n");
				if (gbTcpConnection) {
					memset(gau8ReceivedBuffer, 0, sizeof(gau8ReceivedBuffer));
					sprintf((char *)gau8ReceivedBuffer, "%s", MAIN_PREFIX_BUFFER_RX);
					
					tstrSocketConnectMsg *pstrConnect = (tstrSocketConnectMsg *)pvMsg;
					if (pstrConnect && pstrConnect->s8Error >= SOCK_ERR_NO_ERROR) {
						//printf("send \n");
						send(tcp_client_socket, gau8ReceivedBuffer, strlen((char *)gau8ReceivedBuffer), 0);

						memset(gau8ReceivedBuffer, 0, MAIN_WIFI_M2M_BUFFER_SIZE);
						recv(tcp_client_socket, &gau8ReceivedBuffer[0], MAIN_WIFI_M2M_BUFFER_SIZE, 0);
						
						} else {
						//printf("socket_cb: connect error!\r\n");
						gbTcpConnection = false;
						close(tcp_client_socket);
						tcp_client_socket = -1;
					}
				}
			}
			break;
			


			case SOCKET_MSG_RECV:
			{
				
				char *pcIndxPtr;
				char *pcEndPtr;

				tstrSocketRecvMsg *pstrRecv = (tstrSocketRecvMsg *)pvMsg;
				if (pstrRecv && pstrRecv->s16BufferSize > 0) {

					char *isLockedReceived = strstr(pstrRecv->pu8Buffer, "\"isLocked\":");
					int isLockedReceivedInt = isLockedReceived[11] - '0';

					
					char *enteredReceived = strstr(pstrRecv->pu8Buffer, "\"entered\":");
					int enteredReceivedInt = enteredReceived[10] - '0';
					printf("---------------------\n");
					
					if(enteredReceivedInt != motion){
						printf("Usuario mudou entered\n");
						motion = enteredReceivedInt;
					}
					else{
						printf("isLocked n�o mudou\n");
					}
					
					if(isLockedReceivedInt != door){
						printf("Usuario mudou isLocked no site\n");
						if(!isLockedReceivedInt){
							printf("Abrir porta\n");
							int steps = -200;
							door = isLockedReceivedInt;
							xQueueSend(xQueueMotor, (void*) &steps, 10);
						}
						else{
							printf("Fechar porta\n");
							int steps = 200;
							door = isLockedReceivedInt;
							xQueueSend(xQueueMotor, (void*) &steps, 10);
						}
						
					}
					else{
						printf("entered n�o mudou\n");
					}
					printf("---------------------\n");
					
					
					memset(gau8ReceivedBuffer, 0, sizeof(gau8ReceivedBuffer));
					recv(tcp_client_socket, &gau8ReceivedBuffer[0], MAIN_WIFI_M2M_BUFFER_SIZE, 0);
					
					
					} else {
					//printf("socket_cb: recv error!\r\n");
					close(tcp_client_socket);
					tcp_client_socket = -1;
				}
			}
			break;

			default:
			break;
		}
	}
}

static void socket_cb_tx(SOCKET sock, uint8_t u8Msg, void *pvMsg){
	if(sendBuffer){
		u8Msg = SOCKET_MSG_CONNECT;
	}
	/* Check for socket event on TCP socket. */
	if (sock == tcp_client_socket) {
		switch (u8Msg) {
			case SOCKET_MSG_CONNECT:
			{
				//printf("socket_msg_connect\n");
				if (gbTcpConnection) {
					memset(gau8ReceivedBuffer, 0, sizeof(gau8ReceivedBuffer));
					sprintf((char *)gau8ReceivedBuffer, "GET /setStatus/?islocked=%d&entered=%d HTTP/1.1\r\n Accept: */*\r\n\r\n", door, motion);
					printf("VOU MANDAR MALUCO, isLocked = %d, motion = %d\n", door, motion);

					//sprintf((char *)gau8ReceivedBuffer, "%s", MAIN_PREFIX_BUFFER_RX);

					tstrSocketConnectMsg *pstrConnect = (tstrSocketConnectMsg *)pvMsg;
					if (pstrConnect && pstrConnect->s8Error >= SOCK_ERR_NO_ERROR) {
						//printf("send \n");
						send(tcp_client_socket, gau8ReceivedBuffer, strlen((char *)gau8ReceivedBuffer), 0);

						memset(gau8ReceivedBuffer, 0, MAIN_WIFI_M2M_BUFFER_SIZE);
						recv(tcp_client_socket, &gau8ReceivedBuffer[0], MAIN_WIFI_M2M_BUFFER_SIZE, 0);
						} else {
						//printf("socket_cb: connect error!\r\n");
						gbTcpConnection = false;
						close(tcp_client_socket);
						tcp_client_socket = -1;
						sendBuffer = false;
					}
				}
			}
			break;
			


			case SOCKET_MSG_RECV:
			{
				char *pcIndxPtr;
				char *pcEndPtr;

				tstrSocketRecvMsg *pstrRecv = (tstrSocketRecvMsg *)pvMsg;
				if (pstrRecv && pstrRecv->s16BufferSize > 0) {
					//printf(pstrRecv->pu8Buffer);
					printf("---------------------\n");
					printf("Sent\n");
					printf("---------------------\n");
					
					memset(gau8ReceivedBuffer, 0, sizeof(gau8ReceivedBuffer));
					recv(tcp_client_socket, &gau8ReceivedBuffer[0], MAIN_WIFI_M2M_BUFFER_SIZE, 0);
					} else {
					//printf("socket_cb: recv error!\r\n");
					close(tcp_client_socket);
					tcp_client_socket = -1;
					isTx = false;
				}
			}
			break;

			default:
			break;
		}
	}
	
}

static void socket_cb(SOCKET sock, uint8_t u8Msg, void *pvMsg)
{
	if(isTx){
		socket_cb_tx(sock, u8Msg, pvMsg);
	}
	else{
		socket_cb_rx(sock, u8Msg, pvMsg);
	}
	
}

static void set_dev_name_to_mac(uint8_t *name, uint8_t *mac_addr)
{
	/* Name must be in the format WINC1500_00:00 */
	uint16 len;

	len = m2m_strlen(name);
	if (len >= 5) {
		name[len - 1] = MAIN_HEX2ASCII((mac_addr[5] >> 0) & 0x0f);
		name[len - 2] = MAIN_HEX2ASCII((mac_addr[5] >> 4) & 0x0f);
		name[len - 4] = MAIN_HEX2ASCII((mac_addr[4] >> 0) & 0x0f);
		name[len - 5] = MAIN_HEX2ASCII((mac_addr[4] >> 4) & 0x0f);
	}
}

/**
 * \brief Callback to get the Wi-Fi status update.
 *
 * \param[in] u8MsgType Type of Wi-Fi notification.
 * \param[in] pvMsg A pointer to a buffer containing the notification parameters.
 *
 * \return None.
 */
static void wifi_cb(uint8_t u8MsgType, void *pvMsg)
{
	switch (u8MsgType) {
	case M2M_WIFI_RESP_CON_STATE_CHANGED:
	{
		tstrM2mWifiStateChanged *pstrWifiState = (tstrM2mWifiStateChanged *)pvMsg;
		if (pstrWifiState->u8CurrState == M2M_WIFI_CONNECTED) {
			printf("wifi_cb: M2M_WIFI_CONNECTED\r\n");
			m2m_wifi_request_dhcp_client();
		} else if (pstrWifiState->u8CurrState == M2M_WIFI_DISCONNECTED) {
			printf("wifi_cb: M2M_WIFI_DISCONNECTED\r\n");
			gbConnectedWifi = false;
 			wifi_connected = 0;
		}

		break;
	}

	case M2M_WIFI_REQ_DHCP_CONF:
	{
		uint8_t *pu8IPAddress = (uint8_t *)pvMsg;
		//printf("wifi_cb: IP address is %u.%u.%u.%u\r\n",
				//pu8IPAddress[0], pu8IPAddress[1], pu8IPAddress[2], pu8IPAddress[3]);
		wifi_connected = M2M_WIFI_CONNECTED;
		
    /* Obtain the IP Address by network name */
		//gethostbyname((uint8_t *)server_host_name);
		break;
	}

	default:
	{
		break;
	}
	}
}

void door_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 5);
	
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_EDGE, handler);


};

void motion_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	
	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 5);
	
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);
	
	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_RISE_EDGE, handler);

};

/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */
static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);

	for (;;) {
		printf("--- task ## %u", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		vTaskDelay(1000);
	}
}

static void task_motor(void *pvParameters){
	Stepper n;
	initStepper(&n, 200, MOTOR1_PIO, MOTOR1_PIO_PIN_MASK, MOTOR2_PIO, MOTOR2_PIO_PIN_MASK, MOTOR3_PIO, MOTOR3_PIO_PIN_MASK, MOTOR4_PIO, MOTOR4_PIO_PIN_MASK);
	setSpeed(&n, 60);
	
	xQueueMotor = xQueueCreate(32, sizeof(int));
	if (xQueueMotor == NULL) printf("falha em criar Queue \n");
	int steps_to_move;
	
	while (1){
		if( xQueueReceive(xQueueMotor, &steps_to_move, portMAX_DELAY)){
			vTaskPrioritySet(xHandleStepper, configMAX_PRIORITIES - 1);
			int steps_left = abs(steps_to_move);
			printf("Motor girando...\n", steps_to_move);
			
			if(steps_to_move > 0){n.direction = 1;}
			if(steps_to_move < 0){n.direction = 0;}
			while(steps_left > 0){
				//printf("Motor task, steps_left: %d\n", steps_left);
				if(n.direction == 1){
					n.step_number++;
					if(n.step_number == n.number_of_steps){
						n.step_number = 0;
					}
				}
				else{
					if(n.step_number == 0){
						n.step_number = n.number_of_steps;
					}
					n.step_number--;
				}
				steps_left--;
					
				stepMotor(&n, n.step_number % 4);
				//printf("Step number: %d\n", n.step_number%4);
				delay_us(n.step_delay*10);

			}
			vTaskPrioritySet(xHandleStepper, TASK_WIFI_STACK_PRIORITY);

		}
	}
}

static void task_wifi(void *pvParameters) {
	tstrWifiInitParam param;
	int8_t ret;
	uint8_t mac_addr[6];
	uint8_t u8IsMacAddrValid;
	struct sockaddr_in addr_in;
	
	/* Initialize the BSP. */
	nm_bsp_init();
	
	/* Initialize Wi-Fi parameters structure. */
	memset((uint8_t *)&param, 0, sizeof(tstrWifiInitParam));

	/* Initialize Wi-Fi driver with data and status callbacks. */
	param.pfAppWifiCb = wifi_cb;
	ret = m2m_wifi_init(&param);
	if (M2M_SUCCESS != ret) {
		//printf("main: m2m_wifi_init call error!(%d)\r\n", ret);
		while (1) {
		}
	}
	
	/* Initialize socket module. */
	socketInit();

	/* Register socket callback function. */
	registerSocketCallback(socket_cb, resolve_cb);

	/* Connect to router. */
	//printf("main: connecting to WiFi AP %s...\r\n", (char *)MAIN_WLAN_SSID);
	m2m_wifi_connect((char *)MAIN_WLAN_SSID, sizeof(MAIN_WLAN_SSID), MAIN_WLAN_AUTH, (char *)MAIN_WLAN_PSK, M2M_WIFI_CH_ALL);

	addr_in.sin_family = AF_INET;
	addr_in.sin_port = _htons(MAIN_SERVER_PORT);
	inet_aton(MAIN_SERVER_NAME, &addr_in.sin_addr);
	//printf("Inet aton : %d", addr_in.sin_addr);
	
  while(1){
	  m2m_wifi_handle_events(NULL);

	  if (wifi_connected == M2M_WIFI_CONNECTED) {
		  /* Open client socket. */
		  if (tcp_client_socket < 0) {
			  //printf("socket init \n");
			  if ((tcp_client_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
				  //printf("main: failed to create TCP client socket error!\r\n");
				  continue;
			  }

			  /* Connect server */
			  //printf("socket connecting\n");
			  
			  if (connect(tcp_client_socket, (struct sockaddr *)&addr_in, sizeof(struct sockaddr_in)) != SOCK_ERR_NO_ERROR) {
				  close(tcp_client_socket);
				  tcp_client_socket = -1;
				  //printf("error\n");
				  }else{
				  gbTcpConnection = true;
			  }
		  }
	  }
	  }
}
/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */
int main(void)
{
	/* Initialize the board. */
	sysclk_init();
	board_init();

	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);
	
	door_init(DOOR_SENSOR_PIO, DOOR_SENSOR_PIO_ID, DOOR_SENSOR_PIN, DOOR_SENSOR_PIO_MASK, door_callback);
	motion_init(MOTION_SENSOR_PIO, MOTION_SENSOR_PIO_ID, MOTION_SENSOR_PIN, MOTION_SENSOR_PIO_MASK, motion_callback);
	
	isTx = true;
	
	
	
	if (xTaskCreate(task_wifi, "Wifi", TASK_WIFI_STACK_SIZE, NULL,
	TASK_WIFI_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Wifi task\r\n");
	}
	if (xTaskCreate( task_motor, "Motor", TASK_WIFI_STACK_SIZE, NULL, 
	TASK_WIFI_STACK_PRIORITY, &xHandleStepper) != pdPASS) {
		printf("Failed to create Wifi task\r\n");
	}
	
	
	vTaskStartScheduler();
	
	while(1) {};
	return 0;

}
